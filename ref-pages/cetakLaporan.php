<?php
require_once 'vendor/autoload.php';

ref_function('my-function');

$transaksi = getPenjualanByMonthYear($_POST['bulan'], $_POST['tahun']);


$mpdf = new \Mpdf\Mpdf(['tempDir' => 'ref-files/temp/']);
$bulan = date('M', strtotime("2000-".$_POST['bulan']."-01"));
$tahun = date('Y', strtotime($_POST['tahun']."-01-01"));

$html = '
<html>
<head>
<style type="text/css">
    body {
        font-size: 11pt;
        font-family: Arial;
    }
    h1 {
        padding: 0;
        margin: 0;
        font-size: 12pt;
    }

</style>
</head>
<body>
    <center><b>Laporan Penjualan<br></b></center>
	<br>
	<center>
    Bulan: '.$bulan.' '.$tahun.'
    </center>
    
    <br>
    
    <table width="100%" border="1" cellspacing="0">
        <tr>
            <th>No</th>
            <th>Transaksi</th>
            <th>Tanggal</th>
            <th>Provider</th>
            <th>Produk</th>
            <th>No. HP</th>
            <th>Harga</th>
        </tr>';

if($transaksi != ""):
    $no = 1;
    $total_harga = 0;
    while($row = mysqli_fetch_assoc($transaksi)):
        $total_harga += $row['harga'];
        $html .= '<tr>
        <td>'.$no++.'</td>
        <td>#'.$row['id_transaksi'].'</td>
        <td>'.date("d-m-Y H:i:s ", $row['waktu']).'</td>
        <td>'.$row['produk'].'</td>
        <td>'.$row['kode'].'</td>
        <td>'.substr_replace($row['nomor_telepon'], 'XXX', -3).'</td>
        <td>'.uangRupiah($row['harga']).'</td>
        </tr>';
    endwhile;
    $html .= '<tr><th colspan="6">TOTAL</th><th>'.uangRupiah($total_harga).'</th></tr>';
endif;

$html .= '</table>

    <br>
    <br>


</section>
</body>
</html>

';
$mpdf->WriteHTML($html);
$mpdf->Output();
//echo $html;
?>
