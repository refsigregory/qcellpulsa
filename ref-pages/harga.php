<?php
ref_function('my-function');

$daftarHarga = priceList();
$provider = [];

if($daftarHarga != ""):
foreach ($daftarHarga as $row)
{
    if(!in_array($row->type, $provider))
    	$provider[] = $row->type;
}
endif;

if(isset($_GET['produk']))
{
  $showProduct = $_GET['produk'];
  $data['title'] = "Daftar Harga " . $showProduct . " di " . ref_config("site_name");
} else {
  $showProduct = "PULSA";
  $data['title'] = "Daftar Harga";
}


$voucher = getAllVoucher();

ref_include("web/header", $data);
?>



<div class="container">
  <!-- Content here -->
    <div class="row">
    <div class="col-md-3">
    <ul class="list-group">
      <?php foreach($provider as $row):?>
        <?php if($row != "TOKEN" && $row != "PPOB" && $row != "OTHERS" && $row != "GAME"):?>
        <li class="list-group-item <?=$showProduct == $row ? ' active':'';?>">
        <a  href="?produk=<?=$row;?>"><?=$row;?></a>
      </li>
        <?php endif;?>
    <?php endforeach; ?>
      </ul>
    </div>
    <div class="col-md">
        <?php $no = 1;?>
         <div class="table-responsive">
              <table id="dataTable" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>No</th>
                  <th>Provider</th>
                  <th>Produk</th>
                  <th>Harga</th>
                  <th>Harga Dollar</th>
                  <th>Status</th>
                </tr>
                </thead>
                <tbody>
                <?php 
                if($showProduct != ""):
                  foreach (getDataProdukByType($showProduct) as $row):
                    //foreach ($row->product as $product) {
                ?>
                <tr>
                  <td><?=$no++;?></td>
                  <td><?=$row['nama'];?></td>
                  <td><?=$row['kode'];?></td>
                  <td><?=uangRupiah($row['harga_jual']);?></td>
                  <td><?=uangDollar($row['harga_jual_dollar']);?></td>
                  <td><?=($row['tersedia'] == 1 ? 'Tersedia':'Tidak Tersedia');?></td>

                </tr>
                    <?php //}
                    endforeach;
                endif;
                ?>
                </tbody>
              </table>
              </div>
    </div>
            </div>
</div>

<?php
    ref_include("web/footer");
?>