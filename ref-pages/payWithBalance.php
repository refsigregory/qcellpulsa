<?php
ref_function('my-function');
checkLogin();

if(isset($_POST))
{
    $id = $_POST['trxid'];
    $transaksi = getTransaksiByTrxID($id);
    //$voucher = getVoucherByID($transaksi['id_voucher']);
    $trn_id = $_POST['trn_id'];
    if($transaksi != ""):
        $member = getMemberByIDUser($_SESSION['data']['id']);

        if($member != ""):
            if($member['saldo'] < $transaksi['harga']):
                $error = "Saldo tidak cukup!";
            else:
                $data = ["status_pembayaran" => "success"];
                $query = db_update("transaksi", $data, ["id_transaksi", $transaksi['id_transaksi']]);
                $member = getMemberByID($transaksi['id_pembeli']);
                
                $saldo = $member['saldo'] - $transaksi['harga'];
                db_update("members", ["saldo" => $saldo], ["id_member", $transaksi['id_pembeli']]);

                if($query)
                {
                    // ISI PULSA
                    ref_redir(base_url() . 'api/trx?id=' . $transaksi['id_transaksi']."&dest=".$transaksi['nomor_telepon']."&produk=".$transaksi['kode']);
                    // KEMBALI KE DETAIL ORDER
                    //ref_redir('order?view=' . $transaksi['id_transaksi']);
                } else {
                    $error = "Terjadi Kesalahan " . mysqli_error($db);
                }
            endif;
        else:
            $error = "Anda tidak terdaftar";
        endif;
    else:
        $error = "Pembayaran tidak ditemukan";
    endif;

    if(isset($error)) {
        //$error = implode(". ", $err);
        //print_r($data);
        ref_redir(base_url() . 'order?view='. $transaksi['id_transaksi'] . "&error=". $error);
    }
}
