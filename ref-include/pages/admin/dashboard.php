<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
      </h1>
    </section>
    <!-- Main content -->
    <section class="content">
    <?php if($_SESSION['data']['role'] == 'member'):
      $saldo = 0;
      $getData = getMemberByIDUser($_SESSION['data']['id']);
      if($getData != "")
      {
        $saldo = $getData['saldo'];
      }
    ?>
    <div class="alert alert-info" style="text-align:center;font-weight:bold;">SALDO ANDA RP. <?php echo uangRupiah($saldo);?>,- <a href="<?=base_url();?>akun/cara_deposit" title="Cara Deposit"><i class="fa fa-question-circle"></a></i></div>
    <?php endif;?>
      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h2 class="body-title">Selamat Datang di <?=ref_config("site_name");?>!</h2>
        </div>
        <div class="box-body">
          <div class="alert alert-success">
            Email Paypal Aktif: <b><?=ref_config("paypal_email_aktif");?></b>
          </div>
          <div class="alert alert-success">
            <b>Rekening Aktif</b><br>
            Nama Bank:
            <br>
            <?=ref_config("rekening_bank");?><br>
            Nomor Rekening:
            <br>
            <?=ref_config("rekening_nomor");?><br>
            Atas Nama:
            <br>
            <?=ref_config("rekening_nama");?><br>
          </div>
        </div>
        <!-- /.box-body -->
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->