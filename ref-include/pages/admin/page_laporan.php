<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Laporan Penjualan
      </h1>
    </section>
    <!-- Main content -->
    <section class="content">
<!-- Default box -->
<div class="box">
        <form role="form" action="<?=base_url('cetakLaporan');?>" method="post">
        <div class="box-header with-border">
          <h3 class="box-title">Cetak Laporan</h3>
          
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                    title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body">
                <!-- text input -->
                <div class="form-group">
		<label>Bulan</label>:
		<?php
			$bulan = [
				"Januari",
				"Februari",
				"Maret",
				"April",
				"Mei",
				"Juni",
				"Juli",
				"Agustus",
				"September",
				"Oktober",
        "November",
        "Desember"
			];
		?>
		<select name="bulan" class="form-control">
			<?php for($i=0; $i< count($bulan); $i++):?>
				<option value="<?=$i+1;?>"><?=date("M", strtotime(date("Y") . "-" . ($i+1) . "-" . date("d")));?></option>
			<?php endfor;?>
		</select>
	</div>
	<div class="form-group">
		<label>Tahun</label>:
		<select name="tahun" class="form-control">
			<?php for($tahun = date("Y"); $tahun>2000; $tahun--):?>
				<option><?=$tahun;?></option>
			<?php endfor;?>
		</select>
	</div>

        </div>
        <!-- /.box-body -->
        <div class="box-footer">
            <button class="btn btn-primary btn-block" type="submit">Cetak</button>
        </div>
        <!-- /.box-footer-->
      </form>
      </div>
      <!-- /.box -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->