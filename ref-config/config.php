<?php
/**
 * Site Configuration
 */
$config['site_name']    = "Q-CELL Pulsa";

$config['site_initial'] = "QC";

$config['site_icon'] = "";

$config['site_logo'] = "";

$config['description'] = "Isi Pulsa Online Murah Via Paypal &amp; Bank 24 Jam";

if($_SERVER['HTTP_HOST'] == 'localhost'):
    $config['base_url']     = "http://localhost/Projects/penjualan-pulsa/";
else:
    $config['base_url']     = "https://qcellpulsa.com/";
endif;

$config['default_page'] = "index.php";

/**
 * Information
 */

$config['nomor_kontak'] = '6289642068595'; // awali dengan kode negara, misal negara +62, eh 62 maksudnya

$config['rekening_bank'] = "BRI";

$config['rekening_nomor'] = "0123-456-7891-1234";

$config['rekening_nama'] = "John Doe";

// SET Callback IPN (Instant Payment Notification) ke https://site_url/api/paypal
$config['paypal_email_aktif'] = 'sb-h8qro57537@personal.example.com';

/**
 * API Configuration
 */

$config['keuntungan'] = 2000;

$config['rate_dollar'] = '15000'; // per 1 dollar

$config['dflash_no_hp'] = '089642068595';

$config['dflash_member_id'] = 'DS016736';

$config['dflash_pin'] = '8435';

$config['dflash_password'] = 'qcell*123';
?>