<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Transaksi
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
    <p class="login-box-msg">
          <?php if(isset($_GET['success'])): ?>
             <div class="alert alert-success"><?=$_GET['success'];?></div>
          <?php elseif (isset($_GET['warning'])): ?>
             <div class="alert alert-warning"><?=$_GET['warning'];?></div>
          <?php elseif (isset($_GET['error'])): ?>
             <div class="alert alert-danger"><?=$_GET['error'];?></div>
             <?php elseif (isset($_GET['msg'])): ?>
             <div class="alert alert-info"><?=$_GET['msg'];?></div>
          <?php endif; ?>
    </p>

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Daftar Transaksi</h3>
        </div>
        <div class="box-body">
         <?php $no = 1;?>
         <div class="table-responsive">
              <table id="dataTable" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>No</th>
                  <th>Transaksi</th>
                  <th>Tanggal</th>
                  <th>Provider</th>
                  <th>Produk</th>
                  <th>No. HP</th>
                  <th>Harga</th>
                  <th>Pembayaran</th>
                  <th>Status Pembayaran</th>
                  <th>Status Pengisian</th>
                  <th>Bukti Transaksi</th>

                  <th>Aksi</th>
                </tr>
                </thead>
                <tbody>
                <?php 
                if($transaksi != ""):
                  while($row = mysqli_fetch_assoc($transaksi)):
                ?>
                <tr>
                  <td><?=$no++;?></td>
                  <td>#<?=$row['id_transaksi'];?></td>
                  <td><?=date("d-m-Y H:i:s ", $row['waktu']);?></td>
                  <td><?=$row['produk'];?></td>
                  <td><?=$row['kode'];?></td>
                  <td><?=substr_replace($row['nomor_telepon'], 'XXX', -3);?></td>
                  <td><?=uangRupiah($row['harga']);?></td>
                  <td><?=getPembayaranByID($row['id_pembayaran'])['nama_pembayaran'];?></td>
                  <td><?=ucwords($row['status_pembayaran']);?></td>
                  <td><?=ucwords($row['status_pengisian']);?></td>
                  <td>
                      <?php if($row['bukti_pembayaran'] != ""):?>
                    <a class="label label-info" href="?show_image=<?=$row['bukti_pembayaran'];?>"  title="Lihat Bukti Pembayaran">
                      <i class="fa fa-eye"></i> Lihat Bukti Pembayaran
                    </a>
                      <?php endif;?>
                  </td>
                  <td>
                    <a class="label label-info" href="<?=base_url('');?>order?view=<?=$row['id_transaksi'];?>"  title="Lihat Pesanan">
                      <i class="fa fa-eye"></i>
                    </a>

                    <?php if($_SESSION['data']['role'] == "admin"): ?>
                      <?php if($row['status_pembayaran'] == "pending"): ?>
                    <a class="label label-success" href="<?=base_url('');?>aksi_konfirmasi_transaksi?id=<?=$row['id_transaksi'];?>"  title="Konfirmasi Pembayaran">
                      <i class="fa fa-check"></i>
                    </a>
                      <?php endif;?>

                      <?php if($row['status_pembayaran'] != "refund" && $row['status_pembayaran'] != "success" && $row['status_pembayaran'] != "canceled"): ?>
                    <a class="label label-warning" href="<?=base_url('');?>aksi_refund_transaksi?id=<?=$row['id_transaksi'];?>" title="Refund">
                      <i class="fa fa-refresh"></i>
                    </a>
                      <?php endif;?>

                      <?php if($row['status_pembayaran'] != "canceled" && $row['status_pembayaran'] != "refund" && $row['status_pembayaran'] != "success"): ?>
                    <a class="label label-danger" href="<?=base_url('');?>aksi_cancel_transaksi?id=<?=$row['id_transaksi'];?>" title="Batalkan">
                      <i class="fa fa-close"></i>
                    </a> 
                      <?php endif;?>
                    <?php endif;?>

                      <?php if($row['status_pembayaran'] != "success"): ?>
                    <a class="label label-danger" href="<?=base_url('');?>aksi_hapus_transaksi?id=<?=$row['id_transaksi'];?>" title="Hapus">
                      <i class="fa fa-trash"></i>
                    </a>
                      <?php endif;?>
                  </td>

                </tr>
                  <?php endwhile;
                endif;
                ?>
                </tbody>
              </table>
              </div>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <?php if(isset($_GET['show_image'])) { ?>
  <div class="modal fade" id="show-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Show Image</h4>
      </div>
      <div class="modal-body">
        <img  style="width:100%" src="<?=base_url('ref-files/images/' . $_GET['show_image']);?>" />
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-primary pull-left">Simpan Perubahan</button></form>
        <button type="button" class="btn btn-default pull-right" data-dismiss="modal">Tutup</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<?php } ?>