<?php
ref_function('my-function');
isAdmin();

    if(isset($_GET['id'])) {
        $id = $_GET['id'];
        if(!isset($err))
        {

            $transaksi = getTransaksiByID($id);
            if($transaksi != "")
            {
                if($transaksi['id_pembayaran'] == 1):

                    $member = getMemberByID($transaksi['id_pembeli']);
                    $saldo = $member['saldo'] - $transaksi['harga'];
                    $query = db_update("members", ["saldo" => $saldo], ["id_member", $transaksi['id_pembeli']]);
                else:
                    $query = true;
                endif;

                // ISI PULSA
                //ref_redir(base_url() . 'api/trx?id=' . $transaksi['id_transaksi']."&dest=".$transaksi['nomor_telepon']."&produk=".$voucher['nama_voucher']);
            } else {
                $msg = "Datatransaksi tidak ditemukan";
                $query = false;
            }
            
            if($query){
                db_update("transaksi", ["status_pembayaran" => "success"], ["id_transaksi", $id]);
                $msg = "Data Transaksi Berhasil Dikonfirmasi";
                $_SESION['flashdata'] = array('type' => 'success', 'message' => $msg);
                //ref_redir('akun/transaksi?msg=' . $msg);
                ref_redir(base_url() . 'api/trx?id=' . $transaksi['id_transaksi']."&dest=".$transaksi['nomor_telepon']."&produk=".$voucher['nama_voucher']);
            } else {
                $msg = "Terjadi kesalahan, " . $msg . " " . mysqli_error($db);
                $_SESION['flashdata'] = array('type' => 'success', 'message' => $msg);
                ref_redir('akun/transaksi?msg=' . $msg);
            }
        }else {
            $msg = implode(" ", $err);
            $_SESION['flashdata'] = array('type' => 'error', 'message' => $msg);
            echo $msg;
            ref_redir('akun/transaksi?msg=' . $msg);
        }
    }else {
        $msg = "Data tidak ada";
        $_SESION['flashdata'] =  array('type' => 'error', 'message' => $msg);
        ref_redir('akun/transaksi?msg=' . $msg);
    }

?>