
<footer>
	<div class="container py-3 py-md-4">
		<div class="footer">
				<p class="text-center">© 2019 <?=ref_config("site_name");?>. All Rights Reserved</p>
			</div>
	</div>
</footer>
<!-- footer -->




<!-- js -->
	<script type="text/javascript" src="<?=base_url();?>ref-assets/web/js/jquery-2.2.3.min.js"></script>
	<script type="text/javascript" src="<?=base_url();?>ref-assets/web/js/bootstrap.js"></script> <!-- Necessary-JavaScript-File-For-Bootstrap --> 
	<!-- //js -->

	<script>
		$('#modalVoucher').modal("show");
	</script>

<!-- stats -->
	<script src="<?=base_url();?>ref-assets/web/js/jquery.waypoints.min.js"></script>
	<script src="<?=base_url();?>ref-assets/web/js/jquery.countup.js"></script>
	<script>
		$('.counter').countUp();
	</script>
	<!-- //stats -->
<!-- DataTables -->
<script src="<?=base_url() . REF_PREFIX;?>-assets/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
<script>
  $(function () {
    $('#dataTable').DataTable({
      'paging'      : true,
      'lengthChange': true,
      'searching'   : true,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : true,
      "language": {
                    "sEmptyTable":   "Tidak ada data yang tersedia pada tabel ini",
                    "sProcessing":   "Sedang memproses...",
                    "sLengthMenu":   "Tampilkan _MENU_ entri",
                    "sZeroRecords":  "Tidak ditemukan data yang sesuai",
                    "sInfo":         "Menampilkan _START_ sampai _END_ dari _TOTAL_ entri",
                    "sInfoEmpty":    "Menampilkan 0 sampai 0 dari 0 entri",
                    "sInfoFiltered": "(disaring dari _MAX_ entri keseluruhan)",
                    "sInfoPostFix":  "",
                    "sSearch":       "Cari:",
                    "sUrl":          "",
                    "oPaginate": {
                        "sFirst":    "Pertama",
                        "sPrevious": "Sebelumnya",
                        "sNext":     "Selanjutnya",
                        "sLast":     "Terakhir"
                    }
                }
    });
  })
</script>
 </body>
</html>
