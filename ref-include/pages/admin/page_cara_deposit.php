<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Cara Deposit
      </h1>
    </section>
    <!-- Main content -->
    <section class="content">
      <!-- Default box -->
      <div class="box">
        <div class="box-body">
            <p>Untuk melakukan deposit, Anda bisa langsung mengirim jumlah uang yang akan dideposit ke akun Anda ke salah satu rekening di bawah ini.</p>
            <blockquote>
                Email Paypal Aktif: <b><?=ref_config("paypal_email_aktif");?></b>
            </blockquote>
          <blockquote>
            <b>Rekening Aktif</b><br>
            Nama Bank:
            <br>
            <?=ref_config("rekening_bank");?><br>
            Nomor Rekening:
            <br>
            <?=ref_config("rekening_nomor");?><br>
            Atas Nama:
            <br>
            <?=ref_config("rekening_nama");?><br>
            </blockquote>

            <p>Selanjutnya simpan bukti transaksi Anda dan masuk ke Menu <a href="<?=base_url('akun/tambah_deposit');?>"><i class="fa fa-plus"></i> Deposit</a> dan masukan nominal yang ditransfer beserta bukti transfer dalam bentuk JPG, PNG, GIF atau PDF.</p>
            <p>Saldo anda akan terdeposit jika pembayaran sudah di verifikasi, jika pembayaran ditolak anda harus mengkonfirmasi dan memasitikan bukti transaksi dan jumlah yang dideposit sudah benar.</p>
        </div>
        <!-- /.box-body -->
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->