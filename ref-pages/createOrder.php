<?php
ref_function('my-function');

if(isset($_POST))
{
    $pembayaran = getPembayaranByID($_POST['id_pembayaran']);
    if($pembayaran != ""):
        $nomor_handphone = $_POST['nomor_handphone'];
        //$id_kategori = $_POST['id_kategori'];
        //$id_voucher = $_POST['id_voucher'];
        $kode = $_POST['kode'];
        $jenis_produk = $_POST['produk'];
        $id_pembayaran = $_POST['id_pembayaran'];

        //$voucher = getVoucherByID($id_voucher);
        //$kategori = getKategoriByID($voucher['id_kategori']);
        $produk = getProdukByTypeAndKode($jenis_produk, $kode);
        $member = getMemberByIDUser($_SESSION['data']['id']);

        $data = ["nomor_telepon" => $nomor_handphone,"jenis_produk" => $jenis_produk,  "produk" => $produk["nama"],  "kode" => $kode, "harga" => $produk['harga_jual'], "harga_dollar" => $produk['harga_jual_dollar'], "id_pembayaran" => $id_pembayaran  , "status_pembayaran" => "pending", "waktu" => time()];

        if($_POST['id_pembayaran'] == 1)
        {
            checkLogin();
            if($member != "") {
                $data = array_merge($data, ['id_pembeli' => $member['id_member']]);
            } else {
                $err[] = "Anda belum menjadi member";
            }
        }
        
        if(empty($err)):
            $query = db_insert("transaksi", $data);

            if($query)
            {
                ref_redir('order?view=' . mysqli_insert_id($db));
            } else {
                //ref_redir('');
                $err[] = "Terjadi Kesalahan " . mysqli_error($db);
            }
        endif;
    else:
        $err[] = "Pembayaran tidak ditemukan";
    endif;
    
    if(isset($err)) {
        $error = implode(". ", $err);
        //print_r($data);
        ref_redir(base_url() . '?pesan='. $nomor_handphone .'&produk='.$jenis_produk.'&kode='. $kode . "&error=". $error);
    }
}
