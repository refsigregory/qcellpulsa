<?php
ref_function('my-function');
isAdmin();

    if(isset($_GET['id'])) {
        $id = $_GET['id'];
        if(!isset($err))
        {
            $query = db_delete("deposit", ["id_deposit", $id]);
            if($query){
                $deposit = getDepositByID($id);
                if($deposit != ""):
                    $target_dir = "ref-files/images/";
                    unlink($target_dir . $deposit['bukti_transaksi']);
                endif;
                $msg = "Data Deposit Berhasil Dihapus";
                $_SESION['flashdata'] = array('type' => 'success', 'message' => $msg);
                ref_redir('akun/deposit?msg=' . $msg);
            } else {
                $msg = mysqli_error($db);
                $_SESION['flashdata'] = array('type' => 'success', 'message' => $msg);
                ref_redir('akun/deposit?msg=' . $msg);
            }
        }else {
            $msg = implode(" ", $err);
            $_SESION['flashdata'] = array('type' => 'error', 'message' => $msg);
            echo $msg;
            ref_redir('akun/deposit?msg=' . $msg);
        }
    }else {
        $msg = "Data tidak ada";
        $_SESION['flashdata'] =  array('type' => 'error', 'message' => $msg);
        ref_redir('akun/user?msg=' . $msg);
    }

?>