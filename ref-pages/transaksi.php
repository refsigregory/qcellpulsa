<?php
$data['title'] = "Daftar Transaksi";
ref_function('my-function');
updateTransaksi();

$transaksi = getAllTransaksiTerbaru();

ref_include("web/header", $data);
?>

<div class="container">
  <!-- Content here -->
    <div class="row">
    <div class="col-md">
        <?php $no = 1;?>
         <div class="table-responsive">
              <table id="dataTable" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>No</th>
                  <th>Tanggal</th>
                  <th>Provider</th>
                  <th>Produk</th>
                  <th>No. HP</th>
                  <th>Harga</th>
                  <th>Pembayaran</th>
                  <th>Status Pembayaran</th>
                  <th>Status Pengisian</th>
                </tr>
                </thead>
                <tbody>
                <?php 
                if($transaksi != ""):
                  while($row = mysqli_fetch_assoc($transaksi)):
                ?>
                <tr>
                  <td><?=$no++;?></td>
                  <td><?=date("d-m-Y H:i:s ", $row['waktu']);?></td>
                  <td><?=$row['produk'];?></td>
                  <td><?=$row['kode'];?></td>
                  <td><?=substr_replace($row['nomor_telepon'], 'XXX', -3);?></td>
                  <td><?=uangRupiah($row['harga']);?></td>
                  <td><?=getPembayaranByID($row['id_pembayaran'])['nama_pembayaran'];?></td>
                  <td><?=ucwords($row['status_pembayaran']);?></td>
                  <td><?=ucwords($row['status_pengisian']);?></td>

                </tr>
                  <?php endwhile;
                endif;
                ?>
                </tbody>
              </table>
              </div>
    </div>
            </div>
</div>

<?php
    ref_include("web/footer");
?>