<?php
ref_function('my-function');
updateTransaksi();

$daftarHarga = priceListLocal();
$provider = [];

if($daftarHarga != ""):
foreach ($daftarHarga as $row)
{
    if(!in_array($row->type, $provider))
    	$provider[] = $row->type;
}
endif;

if(isset($_GET['produk']))
{
	$showProduct = $_GET['produk'];
	$data['title'] = "Pengisian " . $showProduct . " Murah";
} else {
	$showProduct = "PULSA";
	$data['title'] = ref_config("site_name");
}


$data['page'] = 'web/page-halaman_utama';

ref_include("web/header", $data);
?>

<!--/banner-->
	<section class="banner-top">
	<div class="banner">
		<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
			<div class="carousel-inner" role="listbox">
				<div class="carousel-item active">
					<div class="carousel-caption">
							<h3><b>ISI PULSA</b> ONLINE MURAH</h3>
						<p>Isi pulsa dengan mudah, harga murah terjangkau.</p>						
					</div>
				</div>
		    </div>
		<!-- banner-bottom -->
	<div class="banner-bottom">
		<div class="container">
			<div class="row">
			<div class="col-lg-12 trans_load_banner_bottom_left">
				<div class="trans-load_banner_bottom_pos">
					<div class="trans_banner_bottom_pos_grid">
						<div class="col-xs-9 trans1_banner_bottom_grid_right">
						<div class="alert alert-info" style="text-align:center;font-weight:bold;">SALDO SERVER RP. <?php echo uangRupiah(getSaldo());?>,-</div>
<ul class="nav nav-tabs" id="daftarProduk" role="tablist">

<?php foreach($provider as $row):?>
	<?php if($row != "TOKEN" && $row != "PPOB" && $row != "OTHERS" && $row != "GAME"):?>
  <li class="nav-item">
    <a  href="?produk=<?=$row;?>" data-provider="<?=$row;?>" aria-controls="<?=$row;?>" <?=$showProduct == $row ? ' class="nav-link active" id="home-tab" aria-selected="true"':' class="nav-link" ';?>><?=$row;?></a>
  </li>
<?php endif;?>
<?php endforeach; ?>
</ul>
<div class="tab-content" id="myTabContent">
<form>

  <div class="tab-pane fade show active" id="pesan" role="tabpanel" aria-labelledby="<?=$row;?>-tab">
		<div class="form-group">
			<?php switch($showProduct){
				case 'TOKEN': ?>
					<input type="text" class="form-control" style="background-color: none" name="pesan" placeholder="ID Pelanggan" >
				<?php break; ?>
				<?php case 'GAME': ?>
					<input type="text" class="form-control" style="background-color: none" name="pesan" placeholder="ID User Game" >
				<?php break; ?>
				<?php default:?>
					<input type="text" class="form-control" style="background-color: none" name="pesan" placeholder="Nomor Handphone" >
				<?php } ?>
		</div>
		<div class="form-group">
			<input type="hidden" name="produk" value="<?=$showProduct;?>">
			<select class="form-control" name="kode">
				<?php
				foreach (getProdukByType($showProduct) as $row) {
				?>
				<optgroup label="<?=$row->title;?>">
				<?php
					foreach ($row->product as $product) {
				?>
					<?php if($product->ready == 1):?>
					<option value="<?=$product->code;?>"<?=$product->ready == 1 ? '' : ' disabled="disabled"';?>><?=$product->name;?></option>
					<?php endif;?>
				<?php
					}
				?>
				</optgroup>
				<?php
				}
				?>
			</select>
		</div>
		<div class="form-group">
			<button class="btn btn-primary">PESAN SEKARANG</button>
		</div>
  </div>
 
</form>
</div>
						</div>
						<div class="clearfix"> </div>
					</div>
				</div>
			</div>
			</div>
			<div class="clearfix"> </div>
		</div>
		</div>
	</div>
	<!-- //banner-bottom -->

	</div>
	</section>
	<!--//banner-->
	<!-- stats -->
	<div class="stats" id="stats">
	<div class="trans-head-all  mb-5">
				<h3></h3>
			</div>
		<div class="container">
			<div class="row">
			<div class="col-md-3  col-sm-6 trans_stats_left trans_counter_grid">
				<span class="fas fa-exchange-alt" aria-hidden="true"></span>
				<p class="counter"><?=getCountAllTransaksi();?></p>
				<h3>Transaksi</h3>
			</div>
			<div class="col-md-3 col-sm-6 trans_stats_left trans_counter_grid1">
				<span class="fas fa-check" aria-hidden="true"></span>
				<p class="counter"><?=getCountAllValidTransaksi();?></p>
				<h3>Transaksi Sukses</h3>
			</div>
			<div class="col-md-3 col-sm-6 trans_stats_left trans_counter_grid2">
				<span class="fas fa-money-bill-alt" aria-hidden="true"></span>
				<p class="counter"><?php  echo getBalance()['trxcount'];?></p>
				<h3>Pembayaran </h3>
			</div>
			<div class="col-md-3 col-sm-6 trans_stats_left trans_counter_grid3">
				<span class="fas fa-dollar-sign" aria-hidden="true"></span>
				<p class="counter"><?php  echo uangRupiah(getBalance()['pemakaian']);?></p>
				<h3>Total Pembayaran </h3>
			</div>
			<div class="clearfix"> </div>
		</div>
		</div>
	</div>
	<!-- //stats -->

<?php if(isset($_GET['pesan']) && isset($_GET['produk'])):?> 
<!-- Modal2 -->
<div class="modal fade" id="modalVoucher" tabindex="-1" role="dialog">
	<div class="modal-dialog">
	<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				
				<div class="signin-form profile">
				<h3 class="transinfo_sign">Pilih Metode Pembayaran</h3>
						<p class="login-box-msg">
							<?php if(isset($_GET['success'])): ?>
								<div class="alert alert-success"><?=$_GET['success'];?></div>
							<?php elseif (isset($_GET['warning'])): ?>
								<div class="alert alert-warning"><?=$_GET['warning'];?></div>
							<?php elseif (isset($_GET['error'])): ?>
								<div class="alert alert-danger"><?=$_GET['error'];?></div>
								<?php elseif (isset($_GET['msg'])): ?>
								<div class="alert alert-info"><?=$_GET['msg'];?></div>
							<?php endif; ?>
						</p>	
						<div class="login-form">
							<form action="<?=base_url('createOrder');?>" method="post">
								<input type="hidden" name="nomor_handphone" value="<?=$_GET['pesan'];?>">
								<input type="hidden" name="produk" value="<?=$_GET['produk'];?>">
								<input type="hidden" name="kode" value="<?=$_GET['kode'];?>">

								<select name="id_pembayaran">
									<?php
										$pembayaran = getAllPembayaran();
									?>
									<?php if($pembayaran != ""): foreach($pembayaran as $row):?>
										<option value="<?=$row['id_pembayaran'];?>" <?=($row['akses_pembayaran'] == "close" && empty($_SESSION['data'])) ? 'disabled="disabled"' : '';?>><?=$row['nama_pembayaran'];?> <?=($row['akses_pembayaran'] == "close") ? '(Hanya Member)' : '';?></option>
									<?php endforeach; endif;?>
								</select>
								<input type="submit" value="PESAN SEKARANG">
							</form>
						</div>
						<p></p>
					</div>
			</div>
		</div>
	</div>
</div>
<!-- //Modal2 -->	
<?php endif;?>

<?php
ref_include("web/footer");
?>
