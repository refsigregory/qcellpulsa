<?php
function checkLogin()
{
    if(!isset($_SESSION['data']))
    {
        ref_redir(base_url() . "login");
    }
}

function isLogin()
{
    if(isset($_SESSION['data']))
    {
        ref_redir(base_url() . "akun");
    }
}

function isAdmin()
{
    if($_SESSION['data']['role'] != "admin")
    {
        ref_redir(base_url() . "akun");
    }
}

function getAllUser()
{
    global $db;
    $query = mysqli_query($db, "select * from users");
    if (mysqli_num_rows($query) > 0)
    {
        return $query;
    } else {
        return "";
    }
}

function getUserByID($id)
{
    global $db;
    $query = mysqli_query($db, "select * from users where id_user = '$id'");
    if (mysqli_num_rows($query) > 0)
    {
        return mysqli_fetch_assoc($query);
    } else {
        return "";
    }
}


function getMemberByID($id)
{
    global $db;
    $query = mysqli_query($db, "select * from members where id_member = '$id'");
    if (mysqli_num_rows($query) > 0)
    {
        return mysqli_fetch_assoc($query);
    } else {
        return "";
    }
}
function getMemberByIDUser($id)
{
    global $db;
    $query = mysqli_query($db, "select * from members where id_user = '$id'");
    if (mysqli_num_rows($query) > 0)
    {
        return mysqli_fetch_assoc($query);
    } else {
        return "";
    }
}


function checkUserByUsername($username)
{
    global $db;
    $query = mysqli_query($db, "select * from users where username = '$username'");
    if (mysqli_num_rows($query) > 0)
    {
        return true;
    } else {
        return false;
    }
}

function checkMemberByEmail($email)
{
    global $db;
    $query = mysqli_query($db, "select * from members where email = '$email'");
    if (mysqli_num_rows($query) > 0)
    {
        return true;
    } else {
        return false;
    }
}


function getPembeliByID($id)
{
    global $db;
    $query = mysqli_query($db, "select * from pembeli where id_pembeli = '$id'");
    if (mysqli_num_rows($query) > 0)
    {
        return mysqli_fetch_assoc($query);
    } else {
        return "";
    }
}

function getPembeliByUserID($id)
{
    global $db;
    $query = mysqli_query($db, "select * from pembeli where id_user = '$id'");
    if (mysqli_num_rows($query) > 0)
    {
        return mysqli_fetch_assoc($query);
    } else {
        return "";
    }
}


function getAllPembayaran()
{
    global $db;
    $query = mysqli_query($db, "select * from pembayaran");
    if (mysqli_num_rows($query) > 0)
    {
        return $query;
    } else {
        return "";
    }
}


function getPembayaranByID($id)
{
    global $db;
    $query = mysqli_query($db, "select * from pembayaran where id_pembayaran = '$id'");
    if (mysqli_num_rows($query) > 0)
    {
        return mysqli_fetch_assoc($query);
    } else {
        return "";
    }
}

function getAllTransaksi()
{
    global $db;
    $query = mysqli_query($db, "select * from transaksi");
    if (mysqli_num_rows($query) > 0)
    {
        return $query;
    } else {
        return "";
    }
}


function getAllTransaksiTerbaru()
{
    global $db;
    $query = mysqli_query($db, "select * from transaksi order by id_transaksi desc");
    if (mysqli_num_rows($query) > 0)
    {
        return $query;
    } else {
        return "";
    }
}

function getCountAllTransaksi()
{
    global $db;
    $query = mysqli_query($db, "select * from transaksi");
    return mysqli_num_rows($query);
}

function getCountAllValidTransaksi()
{
    global $db;
    $query = mysqli_query($db, "select * from transaksi where status_pembayaran = 'success' and status_pengisian = 'success'");
    return mysqli_num_rows($query);
}

function getCountAllPaidTransaksi()
{
    global $db;
    $query = mysqli_query($db, "select * from transaksi where status_pembayaran = 'success'");
    return mysqli_num_rows($query);
}

function getSumAllPaidTransaksi()
{
    global $db;
    $query = mysqli_query($db, "select sum(harga) from transaksi where status_pembayaran = 'success'");
    return mysqli_num_rows($query);
}

function getAllTransaksiByMember($id)
{
    global $db;
    $query = mysqli_query($db, "select * from transaksi where id_pembeli = '$id'");
    if (mysqli_num_rows($query) > 0)
    {
        return $query;
    } else {
        return "";
    }
}


function getTransaksiByID($id)
{
    global $db;
    $query = mysqli_query($db, "select * from transaksi where id_transaksi = '$id'");
    if (mysqli_num_rows($query) > 0)
    {
        return mysqli_fetch_assoc($query);
    } else {
        return "";
    }
}

function getTransaksiByMonthYear($month, $year)
{
    global $db;
    $query = mysqli_query($db, "select * from transaksi where month(FROM_UNIXTIME(waktu)) = '$month' and year(FROM_UNIXTIME(waktu)) = '$year'");
    if (mysqli_num_rows($query) > 0)
    {
        return $query;
    } else {
        return "";
    }
}

function getPenjualanByMonthYear($month, $year)
{
    global $db;
    $query = mysqli_query($db, "select * from transaksi where status_pembayaran = 'success' and month(FROM_UNIXTIME(waktu)) = '$month' and year(FROM_UNIXTIME(waktu)) = '$year'");
    if (mysqli_num_rows($query) > 0)
    {
        return $query;
    } else {
        return "";
    }
}

function getTransaksiByTrxID($trxid)
{
    global $db;
    $query = mysqli_query($db, "select * from transaksi");
    if (mysqli_num_rows($query) > 0)
    {
        $result = "";
        while($data = mysqli_fetch_assoc($query))
        {
            if(sha1(md5(base64_encode($data['id_transaksi']))) == $trxid)
            {    
                $result = $data;
                break;
            }
        }
        if($result == ""):
            return $result;
        else:
            return $result;
        endif;
    } else {
        return "";
    }
}




function getAllVoucher()
{
    global $db;
    $query = mysqli_query($db, "select * from voucher");
    if (mysqli_num_rows($query) > 0)
    {
        return $query;
    } else {
        return "";
    }
}

function getAllVoucherByKategori($id)
{
    global $db;
    $query = mysqli_query($db, "select * from voucher where id_kategori = '$id'");
    if (mysqli_num_rows($query) > 0)
    {
        return $query;
    } else {
        return "";
    }
}

function getVoucherByID($id)
{
    global $db;
    $query = mysqli_query($db, "select * from voucher where id_voucher = '$id'");
    if (mysqli_num_rows($query) > 0)
    {
        return mysqli_fetch_assoc($query);
    } else {
        return "";
    }
}

function getKategoriByID($id)
{
    global $db;
    $query = mysqli_query($db, "select * from kategori where id_kategori = '$id'");
    if (mysqli_num_rows($query) > 0)
    {
        return mysqli_fetch_assoc($query);
    } else {
        return "";
    }
}


function getAllKategori()
{
    global $db;
    $query = mysqli_query($db, "select * from kategori");
    if (mysqli_num_rows($query) > 0)
    {
        return $query;
    } else {
        return "";
    }
}

function getAllDeposit()
{
    global $db;
    $query = mysqli_query($db, "select * from deposit");
    if (mysqli_num_rows($query) > 0)
    {
        return $query;
    } else {
        return "";
    }
}

function getAllDepositByMember($id)
{
    global $db;
    $query = mysqli_query($db, "select * from deposit where id_member = '$id'");
    if (mysqli_num_rows($query) > 0)
    {
        return $query;
    } else {
        return "";
    }
}


function getDepositByID($id)
{
    global $db;
    $query = mysqli_query($db, "select * from deposit where id_deposit = '$id'");
    if (mysqli_num_rows($query) > 0)
    {
        return mysqli_fetch_assoc($query);
    } else {
        return "";
    }
}


function updateTransaksi()
{
    global $db;
    // QUERY untuk otomatis cancel order yang expired (4 jam)
    $query =  mysqli_query($db, "UPDATE transaksi SET status_pengisian = 'failed', status_pembayaran = 'canceled'
    WHERE DATE_ADD(FROM_UNIXTIME(waktu), INTERVAL 4 HOUR) < now() AND status_pembayaran = 'pending'");
}

function generatePassword($string)
{
    return password_hash($string, PASSWORD_DEFAULT);
}

function api_default_signature($memberId, $product = "", $dest = "", $refID = "", $pin, $password) {
    // $template = “OtomaX|” + memberId + “|” + product + “|” + dest + “|” + refID + “|” + pin + “|” + password
    $temp = 'OtomaX|'.$memberId.'|'.$product.'|'.$dest.'|'.$refID.'|'.$pin.'|'.$password.'';
    $template = base64_encode(sha1($temp,true));
    $sign = str_replace('/', '_', str_replace('+', '-' ,rtrim($template, '=')));
    return $sign;
    // sign will be -> z4KNbX-NIUk0_GQb-hMCx17DBCU
}

function getBalance() {
    $sign = api_default_signature(ref_config("dflash_member_id"),"","","",ref_config("dflash_pin"),ref_config("dflash_password"));
    $url = "http://api.dflash.co.id/balance?memberID=".ref_config("dflash_member_id")."&sign=".$sign;
    $data = file_get_contents($url);
    $result = json_decode($data,true);
    if($result['status'] == 20):
        return $result;
    else:
        return "0";
    endif;
}

function getSaldo() {
    $sign = api_default_signature(ref_config("dflash_member_id"),"","","",ref_config("dflash_pin"),ref_config("dflash_password"));
    $url = "http://api.dflash.co.id/balance?memberID=".ref_config("dflash_member_id")."&sign=".$sign;
    $data = file_get_contents($url);
    $result = json_decode($data,true);
    if($result['status'] == 20):
        return $result['saldo'];
    else:
        return 0;
    endif;
}

function priceList() {
    $url = 'https://dflash.co.id/harga/pricelist_json.php';
    $data = file_get_contents($url);
    $result = json_decode($data);
    //$curl = curl_init($url);
    //curl_setopt($curl, CURLOPT_POST, false);
    //curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
    //curl_setopt($curl, CURLOPT_HEADER, false);
    //curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    //$response = curl_exec($curl);
    //$result = json_decode($response);

    // Write to Local
    $myfile = fopen("priceList.json", "w") or die("Unable to open file!");
    fwrite($myfile, $data);
    fclose($myfile);

    return $result;
}

function priceListLocal() {
    $url = base_url(). 'priceList.json';
    $data = file_get_contents($url);
    $result = json_decode($data);
    //$curl = curl_init($url);
    //curl_setopt($curl, CURLOPT_POST, false);
    //curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
    //curl_setopt($curl, CURLOPT_HEADER, false);
    //curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    //$response = curl_exec($curl);
    //$result = json_decode($response);
    return $result;
}

function getProdukByType($type)
{
    $harga = priceListLocal();
    $result = array_filter($harga, function ($item) use ($type) {
        if (stripos($item->type, $type) !== false) {
            return true;
        }
        return false;
    });

    return array_values($result);
}

function getDataProdukByType($type)
{
    global $config;
    $keuntungan = $config['keuntungan'];
    $rate_dollar = $config['rate_dollar'];
    $produk = [];
    foreach (getProdukByType($type) as $row) {
        foreach ($row->product as $product) {
            $produk[] = ["kode" => $product->code, "nama" => $product->name, "harga" => $product->price, "harga_jual" => round($product->price,-3, PHP_ROUND_HALF_DOWN)+$keuntungan, "harga_jual_dollar" => round((round($product->price,-3, PHP_ROUND_HALF_DOWN)+$keuntungan)/$rate_dollar,2), "tersedia" => $product->ready];       
        }
    }
    return $produk;
}

function getProdukByTypeAndKode($type,$kode)
{
    $kode = $kode;
    $data = getDataProdukByType($type);
    $result = [];
    foreach($data as $row)
    {
        if($row['kode'] == $kode)
            $result = $row;
    }
    return $result;
}

function uangRupiah($money)
{
    return str_replace(",", ".", number_format($money));
}

function uangDollar($money)
{
    return "$" . $money;
}
?>