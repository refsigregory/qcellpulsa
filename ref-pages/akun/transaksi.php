<?php
$data['title'] = "Transaksi";
$data['page'] = 'admin/page_transaksi';
ref_function('my-function');
checkLogin();
if($_SESSION['data']['role'] == "admin"):
    $data['transaksi'] = getAllTransaksiTerbaru();
else:
    $data['transaksi'] = getAllTransaksiByMember(getMemberByIDUser($_SESSION['data']['id'])['id_member']);
endif;
ref_include('Loader', $data);
?>