<?php
/**
 * REFramework
 * @author Refsi Gregorius S.
 * @version 0.1.0 (Alpha)
 */

 define('REF_VERSION', '0.1.0');
 define('REF_ENV', 'development'); // development | testing | production
 define('REF_PREFIX', 'ref');

 require_once REF_PREFIX . '-core/loader.php';

 // QUERY untuk otomatis cancel order yang expired (4 jam)
 mysqli_query($db, "UPDATE transaksi SET status_pengisian = 'failed', status_pembayaran = 'canceled'
 WHERE DATE_ADD(FROM_UNIXTIME(waktu), INTERVAL 4 HOUR) < now() AND status_pembayaran = 'pending'");

?>