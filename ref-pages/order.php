<?php
$data['title'] = "Order";
ref_function('my-function');

if(isset($_GET['view']))
{

    $id_transaksi = $_GET['view'];
    $transaksi = getTransaksiByID($id_transaksi);

    if($transaksi == ""):
        ref_redir(base_url());
    endif;

    //$voucher = getVoucherByID($transaksi['id_voucher']);
    //$kategori = getKategoriByID($voucher['id_kategori']);
    $produk = getProdukByTypeAndKode($transaksi['jenis_produk'], $transaksi['kode']);
    $pembayaran = getPembayaranByID($transaksi['id_pembayaran']);
}

if($pembayaran['id_pembayaran'] == 1): // Cuma menampilkan order milik sendiri jika pembayaran lewat saldo
  $saldo = 0;
  $getData = getMemberByIDUser($_SESSION['data']['id']);

  if($getData != "")
  {
    if($getData['id_member'] == $transaksi['id_pembeli']):
      $saldo = $getData['saldo'];
    else:
      ref_redir(base_url());
    endif;
  }
endif;

ref_include("web/header", $data);
?>

<div class="container">
<p class="login-box-msg">
  <?php if(isset($_GET['success'])): ?>
    <div class="alert alert-success"><?=$_GET['success'];?></div>
  <?php elseif (isset($_GET['warning'])): ?>
    <div class="alert alert-warning"><?=$_GET['warning'];?></div>
  <?php elseif (isset($_GET['error'])): ?>
    <div class="alert alert-danger"><?=$_GET['error'];?></div>
    <?php elseif (isset($_GET['msg'])): ?>
    <div class="alert alert-info"><?=$_GET['msg'];?></div>
  <?php endif; ?>
</p>	
  <!-- Content here -->
    <div class="row">
    <div class="col-md">  
              <table class="table">
                <tr>
                  <th colspan="3">Transaksi #<?=$transaksi['id_transaksi'];?></th>
                </tr>
                <tr>
                  <td>Jenis Produk</td>
                  <td><?=$transaksi['jenis_produk'];?></td>
                </tr>
                <tr>
                  <td>Provider</td>
                  <td><?=$transaksi['produk'];?></td>
                </tr>
                <tr>
                  <td>Voucher</td>
                  <td><?=$transaksi['kode'];?></td>
                </tr>
                <tr>
                  <td>Nomor HP</td>
                  <td><?=substr_replace($transaksi['nomor_telepon'], 'XXX', -3);?></td>
                </tr>
                <tr>
                  <td>Harga</td>
                  <td>Rp. <?=uangRupiah($transaksi['harga']);?> | $<?=(double) $transaksi['harga_dollar'];?></td>
                </tr>
                <tr>
                  <td>Pembayaran</td>
                  <td><?=$pembayaran['nama_pembayaran'];?></td>
                </tr>
                <tr>
                  <td>Tanggal Pembelian</td>
                  <td><?=date("H:i:s d-m-Y", $transaksi['waktu']);?></td>
                </tr>
                <tr>
                  <td>Status Pembayaran</td>
                  <td><?=ucwords($transaksi['status_pembayaran']);?></td>
                </tr>
                <tr>
                  <td>Status Pengisian</td>
                  <td><?=ucwords($transaksi['status_pengisian']);?></td>
                </tr>
              </table>
    </div>
    <div class="col-md-4">
              <table class="table">
                <tr>
                  <th>Bayar Dengan <?=$pembayaran['nama_pembayaran'];?></th>
                </tr>
                <tr>
                  <td colspan="2">
                  <?php if($transaksi['id_pembayaran'] == 1):?>
                    Saldo Anda anda saat ini sebesar <?=uangRupiah($saldo);?> akan dikurangi <?=uangRupiah($transaksi['harga']);?>. Sisa saldo Anda setelah melakukan pembayaran akan menjadi <?=uangRupiah($saldo - $transaksi['harga']);?>.
                  <?php elseif($transaksi['id_pembayaran'] == 2):?>
                  <blockquote>
                  <b>Email Paypal Aktif:</b><br><?=ref_config("paypal_email_aktif");?><br>
                  <b>Tipe Pembayaran:</b><br>Teman dan Keluarga<br>
                  <b>Catatan:</b><br><?=$transaksi['id_transaksi'];?><br><br>
                  Masukan nominal yang benar (<b><?=(double) $transaksi['harga_dollar'];?></b>) dengan mata uang <b>USD</b>, Jangan lupa menyertakan nomor transaksi: <b><?=$transaksi['id_transaksi'];?></b> pada Catatan. Tanpa catatan dan nominal yang benar akan menyebabkan pembayaran tidak bisa diproses.
                  </blockquote>
                  <?php elseif($transaksi['id_pembayaran'] == 3):?>
                  <blockquote>
                    <b>Rekening Aktif</b><br>
                    Nama Bank:
                    <br>
                    <?=ref_config("rekening_bank");?><br>
                    Nomor Rekening:
                    <br>
                    <?=ref_config("rekening_nomor");?><br>
                    Atas Nama:
                    <br>
                    <?=ref_config("rekening_nama");?><br>
                    </blockquote>

                    <br>

                    Jangan lupa menginformasikan nomor transaksi anda saat akan mengkonfirmasi pembayaran. Nomor Transaksi Anda adalah <b><?=$transaksi['id_transaksi'];?></b>.
                  </td>
                  <?php endif;?>
                </tr>
                <?php if($transaksi['status_pembayaran'] == 'pending'):?>
                <tr>
                  <td colspan="2"><?=$pembayaran['deskripsi'];?></td>
                </tr>
                <tr>
                  <td>Harga</td>
                  <td>Rp. <?=uangRupiah($transaksi['harga']);?> ($<?=(double) $transaksi['harga_dollar'];?>)</td>
                </tr>
                <tr>
                  <td>Harga Total</td>
                  <td>Rp. <?=uangRupiah($transaksi['harga']);?> ($<?=(double) $transaksi['harga_dollar'];?>)</td>
                </tr>
                <tr>
                  <td colspan="2"><?=$pembayaran['keterangan'];?></td>
                </tr>
                <?php if($transaksi['id_pembeli'] != "" && $transaksi['id_pembayaran'] == 1):?>
                <tr>
                    <td colspan="2">
                        <form action="<?=base_url('payWithBalance');?>" method="post">
                            <input type="hidden" name="trxid" value="<?=sha1(md5(base64_encode($transaksi['id_transaksi'])));?>">
                            <button type="submit" name="submit" class="btn btn-primary btn-block">Konfirmasi Pembayaran</button>
                        </form>
                    </td>
                </tr>
                <?php elseif($transaksi['id_pembayaran'] == 2):?>
                  <form action="<?=base_url('payWithPaypal');?>" method="post">
                  <input type="hidden" name="trxid" value="<?=sha1(md5(base64_encode($transaksi['id_transaksi'])));?>">
                  <td colspan="2">
                    <input type="text" class="form-control" name="trn_id" placeholder="ID Transaksi" required="">
                    <button type="submit" name="submit" class="btn btn-primary btn-block">Konfirmasi Pembayaran</button>
                  </td>
                  </form>
                <?php endif;?>
                <?php elseif($transaksi['status_pembayaran'] == 'success' && $transaksi['status_pengisian'] == 'success'):?>
                    <td colspan="2">
                        <div class="alert alert-success">Pembayaran Sukses</div>
                    </td>
                <?php elseif($transaksi['status_pembayaran'] == 'success' && $transaksi['status_pengisian'] != 'success'): // jika sudah terbayar tapi masih gagal mengisi?>
                  <div class="alert alert-success">Pembayaran Sukses</div>
                  <form action="<?=base_url('payWithPaypal');?>" method="post">
                  <input type="hidden" name="trxid" value="<?=sha1(md5(base64_encode($transaksi['id_transaksi'])));?>">
                  <td colspan="2">
                    <input type="text" class="form-control" name="trn_id" placeholder="ID Transaksi">
                    <button type="submit" name="submit" class="btn btn-primary btn-block">Konfirmasi Pembayaran</button>
                  </td>
                  </form>
                <?php else:?>
                <td colspan="2">
                    <div class="alert alert-danger">Pembayaran Gagal</div>
                </td>
                <?php endif;?>
              </table>
    </div>
    </div>
</div>

<?php
    ref_include("web/footer");
?>